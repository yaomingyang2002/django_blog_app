from django import forms
from .models import Comment


class EmailPostForm(forms.Form): # 1. standard forms
    name = forms.CharField(max_length=25) #default  widget <input type="text"> HTML 
    email = forms.EmailField()
    to = forms.EmailField()
    comments = forms.CharField(required=False,
                               widget=forms.Textarea) #override the <input> widget


class CommentForm(forms.ModelForm): #2. ModelForm
    class Meta:
        model = Comment
        fields = ('name', 'email', 'body')

class SearchForm(forms.Form):  # 3. a standard form for postgres_based fulltext search
    query = forms.CharField()