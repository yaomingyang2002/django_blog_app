from django.shortcuts import render, get_object_or_404 
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView
from django.core.mail import send_mail
from django.db.models import Count
from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank, TrigramSimilarity
from taggit.models import Tag
from .models import Post, Comment 
from .forms import EmailPostForm, CommentForm, SearchForm


# def post_list(request):
def post_list(request, tag_slug=None):   # add an optional tag_slug parameter
    # posts = Post.published.all()
    # return render(request,
    #               'blog/post/list.html',
    #               {'posts': posts})
    object_list = Post.published.all()

    tag = None #  has a None default value

    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug) # get the specified Tag object 
        # filter the list of posts by the given tag, a many-to-many relationship,
        object_list = object_list.filter(tags__in=[tag])

    paginator = Paginator(object_list, 3) #  instantiate the Paginator class with objects displaying 3 posts in each page
    page = request.GET.get('page')  # to get the current page number
    try:
        posts = paginator.page(page) # get posts within the targeted page -- current page
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        posts = paginator.page(paginator.num_pages)
    return render(request,
                  'blog/post/list.html',
                  {'page': page,
                   'posts': posts,
                   'tag': tag})


def post_detail(request, year, month, day, post):
    #get the specified post
    post = get_object_or_404(Post, slug=post,
                                   status='published',
                                   publish__year=year,
                                   publish__month=month,
                                   publish__day=day)
    
    # List of active comments for this post
    comments = post.comments.filter(active=True)
    # process and handle the comment modelForm
    new_comment = None

    if request.method == 'POST':
        # A comment was posted
        comment_form = CommentForm(data=request.POST) # instantiate the form using the submitted data
        if comment_form.is_valid(): # process and validate the form
            # Create Comment object but don't save to database yet
            new_comment = comment_form.save(commit=False)
            # before actually commite, Assign the current post to the comment
            new_comment.post = post
            #finally, Save the post related comment to the database
            new_comment.save()
    else: # if GET, build a form instance 
        comment_form = CommentForm()

    # List of similar posts
    #get tag ID list of the current post
    post_tags_ids = post.tags.values_list('id', flat=True) 
    # get all posts that contain any of these tags,excluding the current post
    similar_posts = Post.published.filter(tags__in=post_tags_ids)\
                                  .exclude(id=post.id) 
    #count the shared tags, order_by shared tags_publish date, retrieve first four posts                             
    similar_posts = similar_posts.annotate(same_tags=Count('tags'))\
                                .order_by('-same_tags','-publish')[:4]

    # saved, or validate errors
    return render(request,
                  'blog/post/detail.html',
                  {'post': post,
                   'comments': comments,
                   'new_comment': new_comment,
                   'comment_form': comment_form,
                   'similar_posts': similar_posts}) #pass the similar posts


class PostListView(ListView):
    queryset = Post.published.all()
    context_object_name = 'posts'
    paginate_by = 3
    template_name = 'blog/post/list.html'


def post_share(request, post_id): 
    # Retrieve post by id to be shared
    post = get_object_or_404(Post, id=post_id, status='published')
    sent = False  # flag hook
    # handling standard EmailPostForms to share post
    if request.method == 'POST': #standard form validate
        # Form was submitted
        form = EmailPostForm(request.POST)
        if form.is_valid(): # use is_valid() to validate the submitted data
            # Form fields passed validation
            cd = form.cleaned_data

            #  send email:
            post_url = request.build_absolute_uri( # get the url of the shared post
                                          post.get_absolute_url())
            subject = '{} ({}) recommends you reading "{}"'.format(cd['name'], cd['email'], post.title)
            message = 'Read "{}" at {}\n\n{}\'s comments: {}'.format(post.title, post_url, cd['name'], cd['comments'])
            send_mail(subject, message, 'admin@myblog.com', [cd['to']]) #use send_mail()
            sent = True # flag hook
    else:
        form = EmailPostForm() # if GET, an empty form instance is displayed
    # after send mail or If the form is not valid, render the form in the template 
    # with the submitted data or/and validation errors
    return render(request, 'blog/post/share.html', {'post': post,
                                                    'form': form,
                                                    'sent': sent
                                                    })


def post_search(request):
    form = SearchForm() # instantiate the SearchForm form
    query = None
    results = []
    if 'query' in request.GET: # To check whether the form is submitted
        form = SearchForm(request.GET) # instantiate form with the submitted GET data
        if form.is_valid():  # verify that the form data is valid
            query = form.cleaned_data['query']
            '''
            #1.search for posts with a custom  SearchVector 
            # instance built with the title and body fields
            # results = Post.objects.annotate(
            #     search=SearchVector('title', 'body'),
            # ).filter(search=query)

            #2. search for posts with SearchQuery for relavance ranking
            search_vector = SearchVector('title', 'body')
            search_query = SearchQuery(query)
            results = Post.objects.annotate(
                        search=search_vector,
                        rank=SearchRank(search_vector, search_query)
                    ).filter(search=search_query).order_by('-rank')
            
            #3. search posts with pecific vectors for query weighting and relavance ranking
            # default D, C, B, A = 0.1, 0.2, 0.4, 1.0
            search_vector = SearchVector('title', 'body')
            search_vector = SearchVector('title', weight='A') + SearchVector('body', weight='B')
            results = Post.objects.annotate(
                        search=search_vector,
                        rank=SearchRank(search_vector, search_query)
                    ).filter(rank__gte=0.3).order_by('-rank')
            '''
            #4.edit the blog/views.py and modify it to search for trigrams.
            results = Post.objects.annotate(
                similarity=TrigramSimilarity('title', query),
            ).filter(similarity__gt=0.3).order_by('-similarity')
    return render(request,
                  'blog/post/search.html',
                  {'form': form,
                   'query': query,
                   'results': results})