from django import template
from ..models import Post
from django.db.models import Count
from django.utils.safestring import mark_safe
import markdown


register = template.Library()

'''
Template tags can process any data and add it to any templates without using views.

django provide:
 simple_tag: Processes the data and returns a string
 inclusion_tag: Processes the data and returns a dictionary of values to target template, which
 is included into any place where the indludion tag is added
'''
@register.simple_tag
def total_posts():
    return Post.published.count()


@register.inclusion_tag('blog/post/latest_posts.html')
def show_latest_posts(count=5):
    latest_posts = Post.published.order_by('-publish')[:count]
    return {'latest_posts': latest_posts}


@register.simple_tag
def get_most_commented_posts(count=5):
    return Post.published.annotate( # use annotate()
               total_comments=Count('comments') #Count aggregation function
           ).order_by('-total_comments')[:count]


@register.filter(name='markdown')
def markdown_format(text):
    return mark_safe(markdown.markdown(text)) #django mark_safe() to mark the result as safe HTML