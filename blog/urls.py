"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import include, path # re_path 
from . import views
from .feeds import LatestPostsFeed


app_name = 'blog' # an application namespace
urlpatterns = [
    # post views
    path('', views.post_list, name='post_list'),
    # path('', views.PostListView.as_view(), name='post_list'),
    path('<int:year>/<int:month>/<int:day>/<slug:post>/', # use path converters to return int or slug from string
         views.post_detail,
         name='post_detail'),
    # re_path(r'^index/$', views.index, name='index'),
    path('<int:post_id>/share/', views.post_share, name='post_share'), #email share url
    path('tag/<slug:tag_slug>/', views.post_list, name='post_list_by_tag'), #use a slug path converter 
    path('feed/', LatestPostsFeed(), name='post_feed'), #instantiate the feed in a new URL pattern
    path('search/', views.post_search, name='post_search'),
] 
# http://127.0.0.1:8000/blog/feed/ 
# http://127.0.0.1:8000/blog/search/