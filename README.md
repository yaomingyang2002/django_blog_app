
Project 1: Building a Blog Application with Django 2.2.5 framework.

This project aim at learning Django and updating to Django 2.2.5 framework by creating a blog application.  
The contents included in this project are about how to:

    1. create the basic blog models, views, templates, and URLs to display blog posts;
    2. build QuerySets with the Django ORM;
    3. configure the Django administration site;
    4. handle forms and model forms;
    5. send emails with Django, and integrate third-party applications; 
    6. implement a comment system for blog posts and allow users to share posts via email;
    7. creating a tagging system;
    8. create custom template tags and filters;
    9. use the sitemap framework and create an RSS feed for the posts; 
    10. complete the blog application by building a search engine with PostgreSQL's full-text search capabilities.

 To run this project:
    clone or download this repository,
    cd to the repository directory,
     setup virtual environment:
        $ pip install virtualenv
        $ virtualenv -p python3 venv
        # To activate venv:
            $ source venv/bin/activate 
        # or to close venv:
            $ deactivate
   
    To install dependencies within venv:
        $ pip install -r requirements.txt 

    $ python manage.py migrate
    $ python manage.py createsuperuser 

    $ python manage.py runserver 
    http://127.0.0.1:8000/blog
    or
    http://127.0.0.1:8000/admin/ 